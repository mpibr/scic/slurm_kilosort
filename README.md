# slurm_kilosort

## activate micromamba
```
$ ssh <username>@slurm.mpibr.local
<username>@lnx-aps-035 ~ $ /gpfs/scic/software/biotools/micromamba/bin/micromamba shell init -s bash -p /gpfs/scic/software/biotools/micromamba
<username>@lnx-aps-035 ~ $ source ~/.bashrc
<username>@lnx-aps-035 ~ $ micromamba --version

```


## prepare virtual environment
```
$ ssh <username>@slurm.mpibr.local
<username>@lnx-aps-035 ~ $ srun --partition gpus --gpus 1 --ntasks 1 --pty bash -i
<username>@lnx-cm-6016 ~ $ micromamba create -p /gpfs/scic/software/biotools/venv_kilosort
<username>@lnx-cm-6016 ~ $ micromamba activate -p /gpfs/scic/software/biotools/venv_kilosort
(venv_kilosort) <username>@lnx-cm-6016 ~ $ micromamba install python=3.9 pytorch pytorch-cuda=11.8 -c pytorch -c nvidia -c conda-forge
(venv_kilosort) <username>@lnx-cm-6016 ~ $ python -m pip install kilosort
```

## prepare configuration

## run kilosort on a gpu node
```
$ ssh <username>@slurm.mpibr.local
<username>@lnx-aps-035 ~ $ cd /path/to/project
<username>@lnx-aps-035 ~ $ sbatch /gpfs/scic/software/slurm_kilosort/pipeline_kilosort.sh recordings.bin
```
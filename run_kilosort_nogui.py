from kilosort import run_kilosort
from kilosort.io import load_probe

# Abreviated arguments, for demonstration only.
# p = load_probe('.../test_prb.prb')
# results = run_kilosort(..., probe=p)

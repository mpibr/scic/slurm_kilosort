#!/bin/bash -ex
#SBATCH --job-name=kilosort
#SBATCH --partition=gpus
#SBATCH --gres=gpu:1
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=8
#SBATCH --error=joblog_error_%A.txt
#SBATCH --output=joblog_output_%A.txt

test_gpu_task() {
    hostname_task=$(hostname)
    echo "Running ${SLURM_ARRAY_TASK_ID} on ${hostname_task} with GPU ${CUDA_VISIBLE_DEVICES}"
}

# set environments
path_biotools="/gpfs/scic/software/biotools"
path_micromamba="${path_biotools}/micromamba/bin"
path_venv="${path_biotools}/venv_kilosort"
path_slurm_kilosort="/gpfs/scic/software/slurm_kilosort"
file_probe="${path_slurm_kilosort}/probe_DiagnosticBiochips.json"

## main
test_gpu_task

## required arguments
file_bin="$1"
if [ ! -f "$file_bin" ]; then
    echo "error: recording BIN file is required."
    exit 1
fi

## create new probe
if [ ! -f "$file_probe" ]; then
    ${path_micromamba}/micromamba run -p ${path_venv} \
    python ${path_slurm_kilosort}/create_kilosort_probe.py
fi

## process data
${path_micromamba}/micromamba run -p ${path_venv} \
python ${path_slurm_kilosort}/run_kilosort_nogui.py \
--bin "$file_bin" \
--probe "$file_probe"


echo "kilosort successfully done."

import argparse
import json
from pathlib import Path
import numpy as np
from kilosort.io import save_probe
import sys


def load_config(config_path):
    if not Path(config_path).exists():
        sys.exit(f"Error: The configuration file '{config_path}' does not exist.")
    with open(config_path, 'r') as file:
        return json.load(file)


# add parser
parser = argparse.ArgumentParser(description="Run Kilosort New Probe")
parser.add_argument('--probe', required=True, help="Path to the probe configuration JSON file")
args = parser.parse_args()

probe_config_path = Path(args.probe)
if not probe_config_path.is_file():
    sys.exit(f"Error: The file '{args.probe}' does not exist or is not a file.")


# read configuration
config_probe = load_config(probe_config_path)
required_keys = ['probe_name', 'n_channels', 'n_columns', 'n_contact_per_column', 'order_perl_columns', 'x_pitch', 'y_pitch', 'tip_to_row']
for key in required_keys:
    if key not in config_probe:
        sys.exit(f"Error: The configuration file is missing the required field '{key}'.")

# get parameters
probe_name = config_probe['probe_name']
n_channels = config_probe['n_channels']
n_columns = config_probe['n_columns']
n_contact_per_column = config_probe['n_contact_per_column']
order_perl_columns = config_probe['order_perl_columns']
x_pitch = config_probe['x_pitch']
y_pitch = config_probe['y_pitch']
tip_to_row = config_probe['tip_to_row']

if len(order_perl_columns) != n_channels:
    sys.exit(f"Error: The length of 'order_perl_columns' ({len(order_perl_columns)}) does not match 'n_channels' ({n_channels}).")

if len(tip_to_row) != n_columns:
    sys.exit(f"Error: The length of 'tip_to_row' ({len(tip_to_row)}) does not match 'n_columns' ({n_columns}).")

# calculate contact coordinates
contacts_x = np.zeros(n_channels)
contacts_y = np.zeros(n_channels)
contacts_x[:n_contact_per_column] = -x_pitch / 2  # Left column
contacts_x[n_contact_per_column:] = x_pitch / 2   # Right column
contacts_y[:n_contact_per_column] = tip_to_row[0] + np.arange(n_contact_per_column) * y_pitch # Left column
contacts_y[n_contact_per_column:] = tip_to_row[1] + np.arange(n_contact_per_column) * y_pitch # Right column

# reorder contacts to match channels
xc = contacts_x[order_perl_columns]
yc = contacts_y[order_perl_columns]

# write probe json file for kilosort
probe = {
    'chanMap': np.arange(n_channels),
    'xc': xc,
    'yc': yc,
    'kcoords': np.zeros(n_channels),  # !!! Assuming 1 shank, modify if multi-shank !!!
    'n_chan': n_channels
}

print(probe)
save_probe(probe, probe_name + ".json")
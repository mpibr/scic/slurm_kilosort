import argparse
import json
import pandas as pd
import spikeinterface.full as si
from pathlib import Path

def load_config(config_path):
    with open(config_path, 'r') as file:
        return json.load(file)

# Set up argument parsing
parser = argparse.ArgumentParser(description="Run Kilosort Preprocessing")
parser.add_argument('--config_paths', required=True, help="Path to the paths configuration JSON file")
parser.add_argument('--config_defaults', required=True, help="Path to the default settings JSON file")
args = parser.parse_args()

# Load configuration files
config_paths = load_config(args.config_paths)
config_default = load_config(args.config_defaults)

# Load Excel file for recordings
excel_df = pd.read_excel(config_paths["input_excel"])

# Process each recording (assuming BIN files are provided)
list_of_recordings = []
for file in excel_df['recordings']:
    recording = si.read_openephys(folder_path=file)
    list_of_recordings.append(recording)

# Concatenate recordings
full_rec = si.concatenate_recordings(list_of_recordings)

# Apply pre-processing filters from configuration
rec_filt = si.highpass_filter(full_rec, **config_default['highpass_filter'])
rec_cmr = si.common_reference(rec_filt, **config_default['common_reference'])

# Save preprocessed data
rec_cmr.save(format='binary', folder=config_paths["kilosort_output"], overwrite=True)
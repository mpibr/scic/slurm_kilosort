# -*- coding: utf-8 -*-
"""
Created on Thu Sep 12 08:12:37 2024

@author: vilarl
"""

import pandas as pd
import torch
from pathlib import Path
import os
import glob
import spikeinterface.full as si
import numpy as np
import matplotlib.pyplot as plt

from probeinterface import generate_multi_columns_probe
from probeinterface.plotting import plot_probe
from probeinterface.io import write_probeinterface, read_probeinterface, write_prb

from kilosort import io, run_kilosort


# Process excel file to convert all filepaths to .posix
def process_excel_file(input_df):
    # Simple heuristic to detect paths; adjust as needed
    def is_path(s):
        return "\\" in s or "/" in s

    # Convert path to posix
    def convert_to_posix(path):
        return Path(path).as_posix()

    # Create a copy of the input dataframe
    output_df = input_df.copy()
    
    # Loop though df cells and substsitute paths
    for col in output_df.columns:
        for index, value in output_df[col].items():
            if isinstance(value, str) and is_path(value):  # Check if the value is a string that looks like a path
                output_df.at[index, col] = convert_to_posix(value)

    # Write the modified data back to an Excel file
    #output_df.to_excel(output_file, index=False)
                
    return output_df

def create_folder(folder_path, folder_name):
    # Combine the base path with the new folder name
    full_path = os.path.join(folder_path, folder_name)
    
    try:
        # Create the folder
        os.makedirs(full_path)
        print(f"Successfully created the directory {folder_path}")
        
    except FileExistsError:
        # If the folder already exists, you can decide to ignore or handle it differently
        print(f"The directory {folder_path} already exists")

    return full_path


def save_txt(variable, folder, filename):
    """
    Saves a given variable's content to a specified file in the given path. The variable can be a string or a list of strings.
    
    Args:
    variable (str or list): The content to be saved. If a list, each element is saved on a new line.
    filename (str): The name of the file where the content will be saved.
    path (str): The directory path where the file will be saved.
    """
    # Combine the path and filename to create the full path
    full_path = os.path.join(folder, filename)
    
    # Open the specified file in write mode ('w'). If the file does not exist, it will be created.
    with open(full_path, 'w') as file:
        if isinstance(variable, list):
            # If the variable is a list, write each element on a new line
            for item in variable:
                file.write(f"{item}\n")
        elif isinstance(variable, str):
            # If the variable is a string, write it directly
            file.write(variable)
        else:
            raise ValueError("Unsupported variable type. Only strings and lists are supported.")
            
            
def get_chans(dataframe, value_column, location):
    """
    Checks that all channel values in a specified column of the excel file are the same for a given location.

    Value_column should match the name used in the excel file
    
    Raises:
    ValueError: If the values in the specified column are not the same for the given location.
    """
    # Step 1: Filter the DataFrame
    subset = dataframe[value_column][dataframe['location'] == location]

    # Step 2: Extract unique values and convert to a list
    unique_values = subset.unique()


    # Step 3 & 4: Check if all values are the same and raise an error if not
    if len(unique_values) != 1:
        print('Check channel numbers in Excel master file')
        raise ValueError(f"Values are not the same: found {unique_values}")
    else:
        print("Same electrode and ADC values for all recordings to be concatenated")
        
    # Convert to an intege
    #unique_values = unique_values.astype(int)
    
    return unique_values            



def generate_channel_ids(num_channels, chan_id, type_channels):
    """
    Generates a list of channel IDs based on the number of channels and a type prefix.

    Parameters:
    - num_channels (int): The number of channels to generate IDs for.
    - type_channels (str): The prefix for each channel ID.

    Returns:
    - list: A list of channel IDs.
    """
    # Convert chan_id to an integer
    chan_id = int(chan_id.item())
    
    if num_channels ==  'multiple_chan':
        # Ensure chan_id is a scalar (to solve some deprecation issue)
        #chan_id = chan_id.item() if chan_id.size == 1 else chan_id
        channels2use = np.arange(chan_id)
        ch_ids = [f'{type_channels}{x+1}' for x in channels2use]
    elif num_channels ==  'one_chan':
        ch_ids = [f'{type_channels}{chan_id}']
    
    return ch_ids



def select_probe(probe_name, display = False):
    
    if probe_name == 'A1x32-Poly2-5mm-50s-177':
    
        #create probe geometry
        #default_probe = get_probe(manufacturer='neuronexus', probe_name='ASSY-37-E-1')
        multi_columns = generate_multi_columns_probe(num_columns=2,
                                                     num_contact_per_column=[16, 16],
                                                     xpitch=43.3, ypitch=50,
                                                     y_shift_per_column=[-750, -775],
                                                     contact_shapes='square', contact_shape_params={'width':12})
        
        #adding the channel mapping to the probe
        chanmap=[7,23,1,28,6,25,14,20,10,22,11,27,5,17,12,21,4,26,3,30,9,19,8,24,13,29,2,18,15,31,0,16]
        col1=chanmap[::2]
        col2=chanmap[1::2]
        col1=col1[::-1]
        col2=col2[::-1]
        channel_indices=np.array(col1+col2)
        multi_columns.set_device_channel_indices(channel_indices)

    
    else:
        print()
        
    if display == True:
        plot_probe(multi_columns, with_contact_id=True)
    elif display == False :
        print('')
        
    return multi_columns


def plot_and_save(trange, recording, save_path, figure_title, display=False):
    """
    Plots the traces for a given time range, saves the figure to the specified path,
    and optionally displays the plot.
    
    Parameters:
    - trange: List of two floats specifying the time range to plot.
    - recording: The recording object to plot.
    - save_path: The path where the figure should be saved.
    - display: Boolean flag to display the plot. Default is True.
    """
    # Create a new figure
    # plt.close('all')
    # matplotlib.use('Agg')
    fig = plt.figure(figsize=(8, 4))
    fig.suptitle(figure_title)
    backend_kwargs = {'figure': fig}
    
    # Plot traces
    si.plot_traces(recording, time_range=trange, backend='matplotlib', **backend_kwargs)

    
    # Save the figure
    fullpath = os.path.join(save_path, figure_title)
    fig.savefig(fullpath)
    
    # Display the figure if the display flag is True
    if display == True:
        plt.show()
    elif display == False:
        plt.close(fig)
        
        
def save_binary(rec, subfolder):
    """
    Saves the recording data in binary format and writes the probe file.

    Parameters:
    - rec: The recording object to be saved.
    - kilosort_subfolder: The path to the folder where the data should be saved.
    """
    # Get datatype for the recording
    #dtype = rec.get_dtype()
    #print(f"Data type for the recording: {dtype}")


    # Define the filename for the traces and the path for the probe
    prb_path = os.path.join(subfolder, 'probe.prb')
    
    # Save the recording in binary format
    saved_rec = rec.save(
        format='binary',
        folder=subfolder,
        overwrite=True,
        progress_bar=True 
    )

    # Save the probe if it is not a TTL channel
    if len(rec.channel_ids) > 1:
        write_prb(prb_path, rec.get_probegroup())
    print(f"Recording and probe saved to: {saved_rec}")        
    
    
def add_to_df(df, column_name, location, value, path):
    """
    Updates or creates a specified column in the DataFrame based on a condition and saves the DataFrame to an Excel file.

    Parameters:
    - df: pd.DataFrame, the DataFrame to be updated
    - location: str, the value in the 'location' column to match
    - rec_path: str, the value to set in the specified column where location matches
    - column_name: str, the name of the column to update or create
    - folder_path: str, the folder path where the updated Excel file will be saved
    """
    # Check if the specified column exists; if not, create it
    if column_name not in df.columns:
        df[column_name] = ''  # Initialize the new column with empty strings

    # Update the specified column where 'location' matches the given location
    df.loc[df['location'] == location, column_name] = value

    # Construct the full path for saving the Excel file
    excel_path = os.path.join(path, 'excel_posix.xlsx')

    # Save the updated DataFrame back to the Excel file (overwrite if exists)
    df.to_excel(excel_path, index=False)
    #print(f"DataFrame has been updated and saved to '{excel_path}'.")    
    
    
    
def convert_path_for_server(input_df, col):
    # Create a copy of the input dataframe
    output_df = input_df.copy()
    for index, value in output_df[col].items():
        output_df.at[index, col] = value.replace("//gpfs.corp.brain.mpg.de", "/gpfs")
    
    return output_df



# def convert_path_for_server(input_df, col):
#     """
#     Convert paths in the specified column of the dataframe based on the provided replacements.

#     Parameters:
#     input_df (pd.DataFrame): The input dataframe.
#     col (str): The column name where the paths need to be converted.
#     replacements (dict): A dictionary where keys are the strings to be replaced and values are the replacement strings.

#     Returns:
#     pd.DataFrame: The dataframe with the paths converted.
#     """
    
#     replacements = {
#         "//gpfs.corp.brain.mpg.de": "/gpfs",
#         r"/gpfs.corp.brain.mpg.de": "/gpfs",
#         "//gpfs": "/gpfs", 
#         r"\\gpfs": r"\gpfs", 
#         r"\\gpfs.corp.brain.mpg.de": r"\gpfs"
#     }
    
#     # Create a copy of the input dataframe
#     output_df = input_df.copy()
    
#     for index, value in output_df[col].items():
#         for old_str, new_str in replacements.items():
#             if old_str in value:
#                 output_df.at[index, col] = value.replace(old_str, new_str)
#             #output_df.at[index, col] = value.replace('//','/')
    
#     return output_df
    

# CODE
#%%
# Import excel file with file organisation
#excel_path = r"\\gpfs.corp.brain.mpg.de\bark\data\2_Share\bark_data\8. Analysis\Lucía\serial codes\data_organisation_2.xlsx"

#excel_path = r"\\gpfs.corp.brain.mpg.de\bark\data\2_Share\bark_data\4. EXPERIMENTS\Excel data organisation\Exp13_Brandybucks_2095_ABN70_08_08_2024.xlsx"
#excel_path = r"\\gpfs.corp.brain.mpg.de\bark\data\2_Share\bark_data\14_CLUSTER\3_jobs\2_kilosort\1_input\test_Exp13_Brandybucks_2095_ABN70_08_08_2024.xlsx"
# HAS TO BE WRITTEN IN CLUSTER FORMAT: '/gpfs/bark/data/
excel_path = '/gpfs/bark/data/2_Share/bark_data/14_CLUSTER/3_jobs/2_kilosort/1_input/WORKSExp13_Brandybucks_2095_ABN70_08_08_2024.xlsx'
kilosort_subfolder = '/gpfs/bark/data/2_Share/bark_data/14_CLUSTER/3_jobs/2_kilosort/2_output/'
#excel_path =r"\\gpfs.corp.brain.mpg.de\bark\data\2_Share\bark_data\14_CLUSTER\3_jobs\2_kilosort\1_input\test_Exp13_Brandybucks_2095_ABN70_08_08_2024.xlsx"
# mode = 'server'
# mode = 'local'

excel_original = pd.read_excel(excel_path)
    
# Convert paths to posix
excel_posix_converted = process_excel_file(excel_original)
save_txt([excel_posix_converted['recordings']], kilosort_subfolder, 'excel_posix_converted.txt')

# Convert paths for server
#excel_server = convert_path_for_server(excel_posix_converted, 'recordings')   
#excel_posix = excel_server
excel_posix = excel_posix_converted
save_txt([excel_posix['recordings']], kilosort_subfolder, 'excel_posix.txt')

kilosort_master_path = '/gpfs/bark/data/2_Share/bark_data/14_CLUSTER/3_jobs/2_kilosort/2_output/'


#%%
# Loop through electrode locations
n_locations = excel_posix['location'].unique()
for location in n_locations:
    
    # Create folder name to save kilosort data
    kilosort_subfolder = create_folder(kilosort_master_path, f"Location_{location}")    
  
    
    # Create a list of files to concatenate and save as a .txt
    list_of_files = excel_posix['recordings'][excel_posix['location'] == location].tolist()
    save_txt(list_of_files, kilosort_subfolder, '1_files_to_concatenate.txt')
    
    print(list_of_files)
    
    # Loop through list of files for one location
    list_of_recordings = []
    for file in list_of_files:
        save_txt([file], kilosort_subfolder, 'file.txt')
        # ELECTRODE CHANNELS
        # Read the file and keep only the specified channels
        print(file)
        auxrec = si.read_openephys(folder_path=file)
        # Append the current recording to the list
        list_of_recordings.append(auxrec)

    save_txt(list_of_recordings, kilosort_subfolder, '2_list_of_recordings_to_concatenate_info.txt')
 
    # Concatenate recordings
    full_rec = si.concatenate_recordings(list_of_recordings)
    print(f'Concatenated recording (electrdodes):\n{full_rec}')
    
    
    #%% Separate electrode and ADC channels
    # Select channels to concatenate
    electrode_channels = get_chans(excel_posix, 'n_electrode_chans', location)
    ADC_channels = get_chans(excel_posix, 'ADC_chan', location)

    # # Generate channel lists
    chan_ids = generate_channel_ids('multiple_chan', electrode_channels, 'CH')
    ADC_ids = generate_channel_ids('one_chan', ADC_channels, 'ADC')
    
    # Get data only for selected channels
    ADC = full_rec.channel_slice(channel_ids=ADC_ids)
    rec = full_rec.channel_slice(channel_ids=chan_ids) 
    
    print(ADC)
    print(rec)
    
    
    #%% Probe
    # Select probe and add to concatenatead recording
    # Reuse function of channels to get probe from excel file
    probe_excel = get_chans(excel_posix, 'probe', location)
    probe = select_probe(probe_excel, display = False)
    # Add probe
    #recording_probe = rec.set_probe(probe, group_mode='by_shank')  
    #rec = rec.set_probe(probe)  
    rec = rec.set_probegroup(probe, group_mode='by_shank')  
    #plot_probe(recording_probe.get_probe())
    
    
    #%% Plot a an excerpt of the data to check the channel mapping
    trange2plot = [10, 10.5]
    plot_and_save(trange2plot, rec, kilosort_subfolder, 'raw_data', display=False)
    
    save_txt([probe], kilosort_subfolder, 'probe.txt')
    
    
    
    #%% Preprocess data
    print( '\n\n\nPRE-PROCESSING' )

    #Let's set a global job_kwargs for spikeinterface
    job_kwargs = dict(n_jobs=50, chunk_duration="1s", progress_bar=True)
    si.set_global_job_kwargs(**job_kwargs) 

    # Bandpass filtering
    rec_filt = si.highpass_filter(rec, freq_min=300)

    #Common median reference
    rec_cmr = si.common_reference(rec_filt, reference='global', operator='median')

    # Plot data
    plot_and_save(trange2plot, rec_cmr, kilosort_subfolder, 'preprocessed_data', display=False)
    

    #%% Save preprocessed data as binary file
    # THIS SHOULD BE SAVED LOCALLY OR TO A HARD DRIVE, AVOID SAVING IN THE SERVER
    # Save electrode data
    save_binary(rec_cmr, kilosort_subfolder)
    
    # Save ADC channel in its own folder
    create_folder(kilosort_subfolder, 'ttl_channel')
    save_binary(ADC, os.path.join(kilosort_subfolder,'ttl_channel'))
    
    # Add data to excel file
    binary_path = Path(os.path.join(kilosort_subfolder,'traces_cached_seg0.raw')).as_posix()
    add_to_df(excel_posix, 'binary_filtered_rec', location, binary_path, kilosort_subfolder)
    add_to_df(excel_posix, 'ttl_path', location, os.path.join(kilosort_subfolder,'ttl_channel'), kilosort_subfolder)
    excel_posix.to_excel(os.path.join(kilosort_subfolder, 'excel_posix.xlsx'))
    
    
    #%% Prepare to run kilosort
    dtype = rec.get_dtype() 
    
    #If parameters are not working check this and adapt accordingly: https://kilosort.readthedocs.io/en/latest/parameters.html
    KS4_PARAMETERS = {
        'nblocks': 0, # no drift correction
        'dmin': 20,
        'dminx': 40,
        'min_template_size': 15, # default is 10
        'nearest_templates': 28,  # 100 is default, should be less than or equal to num_channels
        'nearest_chans': 14, # cannot be larger than total number of channels in probes
        'nt': 91, # 3 ms template length
        'batch_size': 80000,
    }

    settings = {
        'data_dir': kilosort_subfolder,
        'fs': rec.get_sampling_frequency(), 
        'n_chan_bin': rec.get_num_channels()
    }
   
    # update that with the ones I inherited from the wrapper (see params.py for global sorting params):
    settings |= KS4_PARAMETERS
    print('Settings for kilosort:')
    print(settings)
    
    # Save the parameteres here and other thigns that MIGHT be important  into an npy in the sorting folder...
    dict2save = dict(original_parameters=KS4_PARAMETERS)
    np.save(Path(kilosort_subfolder, 'my_params.npy'), dict2save, allow_pickle=True)


# %%Get the probe that we have saved before
    prb_path = os.path.join(kilosort_subfolder,'probe.prb')
    probe = io.load_probe(prb_path)


    # Add path where kilosort output will be saved
    add_to_df(excel_posix, 'ks_output_folder', location, kilosort_subfolder, kilosort_subfolder)
    add_to_df(excel_posix, 'sampling_frequency', location, rec.get_sampling_frequency(), kilosort_subfolder)
    add_to_df(excel_posix, 'dtype', location, dtype, kilosort_subfolder)
    add_to_df(excel_posix, 'num_channels', location, rec.get_num_channels(),kilosort_subfolder)
    excel_posix.to_excel(os.path.join(kilosort_subfolder, 'excel_posix.xlsx'))
    
    #%%
    # Run the algorithm - RUN USING KILOSORT, NOT SPIKEINTERFACE (Francisco says it is much better)
    
    #Force that it works on cuda
    
    # Check the version of torch
    # print(torch.__version__)
    
    # #Lucia: commented because I dont have NVIDIA in my computer
    # device = torch.device('cuda')
    # print(f'Started running kilosort on location {location}')
    # ops, st, clu, tF, Wall, similar_templates, is_ref, est_contam_rate, kept_spikes = run_kilosort(
    #     settings=settings, probe=probe, filename=binary_path, data_dtype=dtype, 
    #     do_CAR=True, device=torch.device('cuda')
    #     )
    
    #Without cuda
    ops, st, clu, tF, Wall, similar_templates, is_ref, est_contam_rate, kept_spikes = run_kilosort(
        settings=settings, probe=probe, filename=binary_path, data_dtype=dtype, 
        do_CAR=True
        )

    
    print(f'Finished processing location {location}')